# Laravel Werf CI/CD Application Helm Chart

## Configuration

### Chart specific configuration 📄

| Parameter        | Description                                              | Default         |
|------------------|----------------------------------------------------------|-----------------|
| nameOverride     | Set name of release                                      | `backend-chart` |
| env              | Set map of environment variables                         |                 |
| _env.APP_DEBUG_  | _An example of environment variable_                     | `"true"`        |
| dockerconfigjson | An Docker configuration JSON for download images ability | `""`            |

### Application configuration 🧮

| Parameter                              | Description                                                                                                      | Default                                                                                 |
|----------------------------------------|------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| **Generic application configs** 🕹     |                                                                                                                  |                                                                                         |
| application.generic.track              | Name of default (generic) track. Used only if specific (app, runner, worker) track is not set                    | `stable`                                                                                |
| application.generic.tier               | Name of default (generic) tier. Used only if specific (app, runner, worker) tier is not set                      | `web`                                                                                   |
| application.generic.replicaCount       | Number of default (generic) replicas. Used only if specific (app, runner, worker) replica count is not set       | `1`                                                                                     |
| application.generic.probes             | Map of default (generic) probes. Used only if specific (app, runner, worker) is not set                          |                                                                                         |
| application.generic.resources          | Map of default (generic) resources. Used only if specific (app, runner, worker) is not set                       |                                                                                         |
| **Default application configs** 🕹     |                                                                                                                  |                                                                                         |
| application.app.enabled                | Set is app deployment enabled                                                                                    | `true`                                                                                  |
| application.app.replicaCount           | Number of replicas for app deployment                                                                            | generics default                                                                        |
| application.app.track                  | Name of app track. Rewrites generic trac                                                                         | `stable`                                                                                |
| application.app.command                | Set array of commands that must be executed for starting pod                                                     | `["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord-app.conf"]`         |
| application.app.tier                   | Name of tier. Rewrites generic tier                                                                              | `"web"`                                                                                 |
| application.app.env                    | Map of deployment environment variables                                                                          |                                                                                         |
| **Migration configs** 🕹               |                                                                                                                  |                                                                                         |
| application.migration.enabled          | Set is migration job enabled                                                                                     | `true`                                                                                  |
| application.migration.command          | Set array of commands that must be executed for migration.                                                       | `["/usr/bin/php", "/app/artisan", "migrate", "--force"]`                                |
| application.migration.tier             | Name of tier. Rewrites generic tier                                                                              | `"migration"`                                                                           |
| application.migration.env              | Map of job environment variables                                                                                 |                                                                                         |
| **Worker configs** 🕹                  |                                                                                                                  |                                                                                         |
| application.worker.enabled             | Set is worker deployment enabled                                                                                 | `true`                                                                                  |
| application.worker.replicaCount        | Number of replicas for worker deployment                                                                         | generics default                                                                        |
| application.worker.track               | Name of worker track. Rewrites generic trac                                                                      | `stable`                                                                                |
| application.worker.command             | Set array of commands that must be executed for starting pod                                                     | `["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord-worker.conf"]`      |
| application.worker.tier                | Name of tier. Rewrites generic tier                                                                              | `"worker"`                                                                              |
| application.worker.env                 | Map of deployment environment variables                                                                          |                                                                                         |
| **Runner configs** 🕹                  |                                                                                                                  |                                                                                         |
| application.runner.enabled             | Set is runner deployment enabled                                                                                 | `true`                                                                                  |
| application.kubernetes.leaderelections | Set is leaderelections enabled. If true, creates leases for leaderelections and enabled changes of replica count | `false`                                                                                 |
| application.runner.replicaCount        | Number of replicas for runner deployment                                                                         | if application.runner.kubernetes.leaderelections is false, 1, otherwise generic default |
| application.runner.track               | Name of runner track. Rewrites generic trac                                                                      | `stable`                                                                                |
| application.runner.command             | Set array of commands that must be executed for starting pod                                                     | `["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord-runner.conf"]`      |
| application.runner.tier                | Name of tier. Rewrites generic tier                                                                              | `"runner"`                                                                              |
| application.runner.env                 | Map of deployment environment variables                                                                          |                                                                                         |

### Service configuration 🔗

| Parameter            | Description                          | Default     |
|----------------------|--------------------------------------|-------------|
| service.enabled      | Enables of "Service" configuration   | `true`      |
| service.annotations  | Map of annotations for "Service"     | `{}`        |
| service.name         | Name of internal port in deployments | `web`       |
| service.type         | Type of Service                      | `ClusterIP` |
| service.externalPort | External port of "Service" endpoint  | `5000`      |
| service.internalPort | Internal port of "Service" endpoint  | `5000`      |

### Ingress configuration 🕸️

| Parameter                         | Description                          | Default                                                            |
|-----------------------------------|--------------------------------------|--------------------------------------------------------------------|
| ingress.enabled                   | Enables of "Ingress" configuration   | `true`                                                             |
| ingress.url                       | Default ingress url                  | `""`                                                               |
| ingress.additonalHosts            | Array of additional urls for ingress | `[]`                                                               |
| ingress.tls.enabled               | Enables of TLS configuration         | `true`                                                             |
| intress.tls.secretName            | Name of TLS secret                   | `""`                                                               |
| ingress.annotations               | Map of annotations for ingress       | Application specific map, check sources for additional information |
| ingress.modSecurity.enabled       |                                      | `false`                                                            |
| ingress.modSecurity.secRuleEngine |                                      | `"DetectionOnly"`                                                  |

### HPA configuration 📈
##### [Check information about HPA configuration](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)
##### [Werf docs about HPA](https://ru.werf.io/guides/golang/300_deployment_practices/030_resource_management.html#horizontalpodautoscaler)

| Parameter                              | Description                                                  | Default |
|----------------------------------------|--------------------------------------------------------------|---------|
| hpa.enabled                            | Enables HPA configuration                                    | `false` |
| hpa.app.enabled                        | Enables HPA app configuration                                | `false` |
| hpa.app.minReplicas                    | Minimum number of replicas that targets HPA                  | `3`     |
| hpa.app.maxReplicas                    | Maximum number of replicas that targets HPA                  | `5`     |
| hpa.app.targetCPUUtilizationPercentage | Target CPU utilization percentage for scaling app deployment | `80`    |
| hpa.worker                             | Map Specific to `hpa.app` configuration                      |         |
| hpa.runner                             | Map Specific to `hpa.app` configuration                      |         |

### PDB configuration 📌
##### [Check information about PDB configuration](https://kubernetes.io/docs/concepts/workloads/pods/disruptions/#pod-disruption-budgets)
##### [Werf docs about PDB](https://ru.werf.io/guides/django/300_deployment_practices/020_high_availability.html#poddisruptionbudget)
 

| Parameter                 | Description                                                            | Default |
|---------------------------|------------------------------------------------------------------------|---------|
| pdb.enabled               | Enables PDB configuration                                              | `false` |
| pdb.app.enabled           | Enables PDB app configuration                                          | `false` |
| pdb.app.minAvailable ⚠️   | Minimum number of replicas that MUST BE available for app deployment   | ``      |
| pdb.app.maxUnavailable ⚠️ | Maximum number of replicas that MUST BE unavailable for app deployment | ``      |
| pdb.worker                | Map Specific to `pdb.app` configuration                                |         |
| pdb.runner                | Map Specific to `pdb.app` configuration                                |         |

###### ⚠️⚠️⚠️ You MUST check, that **`minAvailable`** and **`maxUnavailable`** not both set. If it IS, template WILL follow for `pdb.app.maxUnavailable` configuration as MAJOR rule

## Dependencies configuration 🎛

### PostgreSQL
##### Follow specific documentation [here](https://artifacthub.io/packages/helm/bitnami/postgresql/11.6.5)
| Parameter                   | Description                                  | Default    |
|-----------------------------|----------------------------------------------|------------|
| postgresql.enabled          | Enables PostgreSQL configuration             | `true`     |
| postgresql.fullnameOverride | Rename of PostgreSQL helm dependency release | `postgres` |

### Redis
##### Follow specific documentation [here](https://artifacthub.io/packages/helm/bitnami/redis/17.10.1)
| Parameter              | Description                                 | Default     |
|------------------------|---------------------------------------------|-------------|
| redis.enabled          | Enables Redis configuration                 | `false`     |

### KeyDB
##### Follow specific documentation [here](https://artifacthub.io/packages/helm/enapter/keydb/0.39.0)
| Parameter              | Description                             | Default |
|------------------------|-----------------------------------------|---------|
| keydb.enabled          | Enables KeyDB configuration             | `false` |
| keydb.fullnameOverride | Rename of KeyDB helm dependency release | `keydb` |

### RabbitMQ
##### Follow specific documentation [here](https://artifacthub.io/packages/helm/bitnami/rabbitmq/10.1.1)
| Parameter                 | Description                                | Default    |
|---------------------------|--------------------------------------------|------------|
| rabbitmq.enabled          | Enables RabbitMQ configuration             | `false`    |
| rabbitmq.fullnameOverride | Rename of RabbitMQ helm dependency release | `rabbitmq` |

### Centrifugo
##### Follow specific documentation [here](https://github.com/centrifugal/helm-charts/tree/master/charts/centrifugo)
| Parameter                          | Description                                                                                                       | Default                            |
|------------------------------------|-------------------------------------------------------------------------------------------------------------------|------------------------------------|
| centrifugo.enabled                 | Enables Centrifugo configuration                                                                                  | `false`                            |
| centrifugo.fullnameOverride        | Rename of Centrifugo helm dependency release                                                                      | `centrifugo`                       |
| centrifugo.ingress.hosts[]         | Array of configutation of ingress hosts for centrigugo (Its need to be set to connect centrifugo to your project) |                                    |
| centrifugo.ingress.hosts[].host    | The host name (DNS)                                                                                               | `centrifugo-c.ingress.domain.test` |
| centrifugo.ingress.hosts[].paths[] | Default path                                                                                                      | `["-"]`                            |