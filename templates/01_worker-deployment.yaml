{{- if .Values.application.worker.enabled -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "trackableappname" . }}-worker
  labels:
    app: {{ template "appname" . }}-worker
    track: "{{ default .Values.application.generic.track .Values.application.worker.track }}"
    tier: "{{ default .Values.application.generic.tier .Values.application.worker.tier }}"
    chart: "{{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}"
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  replicas: {{ default .Values.application.generic.replicaCount .Values.application.worker.replicaCount }}
  selector:
    matchLabels:
      app: {{ template "appname" . }}
      track: "{{ default .Values.application.generic.track .Values.application.worker.track }}"
      tier: "{{ default .Values.application.generic.tier .Values.application.worker.tier }}"
      release: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app: {{ template "appname" . }}
        track: "{{ default .Values.application.generic.track .Values.application.worker.track }}"
        tier: "{{ default .Values.application.generic.tier .Values.application.worker.tier }}"
        release: {{ .Release.Name }}
    spec:
      imagePullSecrets:
        - name: "registry-secret"
      {{- include "wait_for" . | nindent 6 }}
      containers:
{{- if .Values.pgbouncer.enabled }}
      - name: pgbouncer
        image: {{ .Values.pgbouncer.image }}
        imagePullPolicy: IfNotPresent
        livenessProbe:
          tcpSocket:
            port: 5432
          periodSeconds: 60
        lifecycle:
          preStop:
            exec:
              # Allow existing queries clients to complete within 120 seconds
              command: ["/bin/sh", "-c", "killall -INT pgbouncer && sleep 120"]
        env:
{{- if eq (include "isSecretExists" . ) "true" }}
          - name: DATABASE_URL
            valueFrom:
              secretKeyRef:
                name: {{ template "trackableappname" . }}-secret
                key: DATABASE_URL
{{- else }}
          - name: DATABASE_URL
            value: {{ .Values.env.DATABASE_URL | quote }}
{{- end }}
          - name: AUTH_TYPE
            value: "any"
{{- end }}
      - name: {{ .Chart.Name }}
        image: {{ list . "worker" | include "werfimage" }}
        command: {{ toYaml .Values.application.worker.command | nindent 10 }}
        imagePullPolicy: IfNotPresent
{{- if eq (include "isSecretExists" . ) "true" }}
        envFrom:
          - secretRef:
              name: {{ template "trackableappname" . }}-secret
{{- end }}
        env:
        - name: APP_PORT
          value: {{ .Values.service.internalPort | quote }}
{{- if .Values.pgbouncer.enabled }}
        - name: DATABASE_URL
          value: "postgresql://db:db@localhost:5432/db"
{{- end }}
{{- if eq (include "isSecretExists" . ) "false" }}
{{- range $name, $value := ( include "pgbouncerRewriter"  . | fromYaml ) }}
        - name: {{ $name }}
          value: {{ $value | quote }}
{{- end }}
{{- else }}
{{- range $name, $value := .Values.env }}
        - name: {{ $name }}
          value: {{ $value | quote }}
{{- end }}
{{- end }}
{{- range $name, $value := .Values.application.worker.env }}
        - name: {{ $name }}
          value: {{ $value | quote }}
{{- end }}
        ports:
        - name: "{{ .Values.service.name }}"
          containerPort: {{ .Values.service.internalPort }}
{{ default .Values.application.generic.probes .Values.application.worker.probes | toYaml | indent 8 }}
        resources:
{{ default .Values.application.generic.resources .Values.application.worker.resources | toYaml | indent 10 }}
{{- if .Values.application.worker.affinity.enabled }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - '{{ template "appname" . }}'
                    - key: tier
                      operator: In
                      values:
                        - '{{ default .Values.application.generic.tier .Values.application.worker.tier }}'
                topologyKey: kubernetes.io/hostname
              weight: 10
{{- end }}
{{- end -}}

